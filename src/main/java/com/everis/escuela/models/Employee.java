package com.everis.escuela.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "employee")
public class Employee extends AuditModel {

    private static final long serialVersionUID = 1L;

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    
    @Column(length=50, name="firstname", nullable=false)
    private String firstName;
    
    @Column(length=50, name="lastname", nullable=false)
    private String lastName;
    
    @Column(length=11, name="documentnumber", nullable=false)
    private String documentNumber;
    
    @Column(length=20, name="email")
    private String email;

}
